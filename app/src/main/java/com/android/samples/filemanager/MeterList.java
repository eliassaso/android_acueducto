package com.android.samples.filemanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MeterList extends Activity {

    private static final String PASSWORD = "123456";
    public Record nextRecord;
//    public String data;
    private TextView txtRoute;
    private TextView txtPassword;
    private TextView txtPreviousConsumption;
    private TextView lblMeter;
    private TextView lblName;
    private TextView lblAddress;
    private EditText txtPreviousReading;
    private EditText txtCurrentReading;
    private EditText txtConsumption;
    private EditText txtSearchMeter;
    private CheckBox cbxValidateMaximumReading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        cbxValidateMaximumReading = findViewById(R.id.valida_lectura_maxima);
        TextView txtPeriod = findViewById(R.id.lbl_periodo);
        txtRoute = findViewById(R.id.lbl_ruta);
        txtPassword = findViewById(R.id.txt_password);
        txtSearchMeter = findViewById(R.id.txt_buscar_medidor);
        lblMeter = findViewById(R.id.lbl_medidor);
        lblName = findViewById(R.id.lbl_nombre);
        lblAddress = findViewById(R.id.lbl_direccion);
        txtPreviousReading = findViewById(R.id.txt_lectura_anterior);
        txtCurrentReading = findViewById(R.id.txt_lectura_actual);
        txtPreviousConsumption = findViewById(R.id.txt_consumo_anterior);

        txtCurrentReading.requestFocus(); // keyboard is activated on this input
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        txtConsumption = findViewById(R.id.txt_consumo);
        updatedConsumption();

        String data = getIntent().getStringExtra(Route.searchFlag ? "dataSearch" : "data");
        assert data != null;
        String[] dataList = data.split(",");

        if (Route.searchFlag) {
            txtPeriod.setText(dataList[0] + " - " + dataList[1]);
            txtRoute.setText("");
        } else {
            txtPeriod.setText(dataList[1] + " - " + dataList[2]);
            txtRoute.setText(dataList[0]);
            next(Integer.parseInt(dataList[0].trim()));
        }
    }

    private void updatedConsumption() {
        txtCurrentReading.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateReadings();
            }
        });
    }

    public void next(int pRuta) {
        clearFields();

        SQLHelper obj = new SQLHelper(this, null);
        nextRecord = obj.nextRecord(pRuta);

        if (nextRecord != null) {
            populateFields(nextRecord);
        } else {
            showNoRecordsToast();
            finish();
        }
    }

    private void clearFields() {
        txtCurrentReading.setText("");
        txtConsumption.setText("");
        txtSearchMeter.setText("");
        txtPreviousConsumption.setText("");
    }

    private void populateFields(Record record) {
        lblMeter.setText(record.getMeterNumber());
        lblName.setText(record.getName());
        lblAddress.setText(record.getAddress());
        txtPreviousReading.setText(String.valueOf(record.getLastReading()));
        txtPreviousConsumption.setText(record.getPreviousConsumption());
        txtCurrentReading.setText(record.getCurrentReading() == -1 ? "" : String.valueOf(record.getCurrentReading()));
    }

    private void showNoRecordsToast() {
        Toast toast2 = Toast.makeText(getApplicationContext(),
                "No se encontraron más registros en ruta", Toast.LENGTH_SHORT);
        toast2.setGravity(Gravity.TOP | Gravity.CENTER, 0, 200);
        toast2.show();
    }

    public void save(View view) {
        if (Route.searchFlag && txtPreviousReading.getText().toString().isEmpty()) {
            showToast("Buscar medidor!!!");
            return;
        }

        if (txtCurrentReading.getText().toString().isEmpty()) {
            showToast("Digitar lectura actual!!");
            return;
        }

        int currentReading = Integer.parseInt(txtCurrentReading.getText().toString());
        int previousReading = Integer.parseInt(txtPreviousReading.getText().toString());

        if (!cbxValidateMaximumReading.isChecked() && currentReading < previousReading) {
            showToast("Lectura actual no debe ser menor a la anterior!! marque validar para proceder.");
            return;
        }

        SQLHelper obj = new SQLHelper(this, null);
        boolean response = obj.save(lblMeter.getText().toString(), currentReading);

        if (response) {
            showToast("Guardado correctamente!!");
            cbxValidateMaximumReading.setChecked(false);
            resetFields();
        } else {
            showToast("Ocurrió un problema con la base de datos, intentar nuevamente!!");
            resetFields();
        }
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 200);
        toast.show();
    }

    private void resetFields() {
        if (Route.searchFlag) {
            clearGeneralFields();
        } else {
            next(Integer.parseInt(txtRoute.getText().toString().trim()));
        }
    }

    private void clearGeneralFields() {
        txtCurrentReading.setText("");
        txtConsumption.setText("");
        txtSearchMeter.setText("");
        txtRoute.setText("");
        lblMeter.setText("medidor");
        lblName.setText("nombre");
        lblAddress.setText("dirección");
        txtPreviousReading.setText("");
        txtPreviousConsumption.setText("");
    }

    public void validateReadings() {
        if (txtPreviousReading.getText().toString().isEmpty() || txtCurrentReading.getText().toString().isEmpty()) {
            return;
        }

        int previousReading = Integer.parseInt(txtPreviousReading.getText().toString());
        int currentReading = Integer.parseInt(txtCurrentReading.getText().toString());

        int consumption;
        if (currentReading < previousReading) {
            consumption = nextRecord.getMaximumReading() - previousReading + currentReading;
        } else {
            consumption = currentReading - previousReading;
        }

        txtConsumption.setText(String.valueOf(consumption));
    }

    public void deleteTable(View v) {
        if (!txtPassword.getText().toString().equals(PASSWORD)) {
            showToast("Contraseña incorrecta!!");
        } else {
            SQLHelper obj = new SQLHelper(this, null);
            obj.deleteAllRecords();
            finish();
        }
    }

    public void search(View v) {
        if (Route.searchFlag && txtSearchMeter.getText().toString().isEmpty()) {
            clearGeneralFields();
        } else {
            SQLHelper obj = new SQLHelper(this, null);
            nextRecord = obj.search(txtSearchMeter.getText().toString());
            if (nextRecord != null) {
                populateFields(nextRecord);
            } else {
                showToast("No se encontraron registros");
                if (Route.searchFlag) {
                    clearSearchFields();
                } else {
                    next(Integer.parseInt(txtRoute.getText().toString().trim()));
                }
            }
        }
    }

    private void clearSearchFields() {
        clearGeneralFields();
        txtCurrentReading.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //un listener que al pulsar, cierre la aplicacion
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Salir")
                    .setMessage("Estás seguro?")
                    .setNegativeButton(android.R.string.cancel, null)//sin listener
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        Route.searchFlag = false;
                        Intent intent = new Intent(MeterList.this, Route.class);
                        startActivity(intent);
                        finish();

                    })
                    .show();
            // Si el listener devuelve true, significa que el evento esta procesado, y nadie debe hacer nada mas
            return true;
        }
        //para las demas cosas, se reenvia el evento al listener habitual
        return super.onKeyDown(keyCode, event);

    }



}
