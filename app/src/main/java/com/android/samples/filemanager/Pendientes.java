package com.android.samples.filemanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class Pendientes extends Activity {

    protected ListView pendientes;
    public static ArrayAdapter<String> adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendientes);

        SQLHelper helper= new SQLHelper(this, null);
        pendientes = findViewById(R.id.lista_pendientes);
        List<Integer> pend = helper.searchPendingRoutes();

        if(pend == null){
            Toast toast2 =
                    Toast.makeText(getApplicationContext(),
                            "No hay rutas pendientes....", Toast.LENGTH_SHORT);

            toast2.setGravity(Gravity.TOP| Gravity.CENTER,0,200);
            toast2.show();
            finish();

        }else {

            java.util.List<String> p = new ArrayList<>();

            for (int i = 0; i < pend.size(); i++) {
                p.add("   Ruta : " + pend.get(i).toString());
            }

            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, p);
            pendientes.setAdapter(adapter);

            pendientes.setOnItemClickListener((pariente, view, posicion, id) -> {
                String elegido = (String) pariente.getItemAtPosition(posicion);

                String data = getIntent().getStringExtra("data");

                String[] datos = elegido.split(":");

                data = datos[1] + "," + data;

                Route.flag = true;
                Intent intent = new Intent(Pendientes.this, MeterList.class);
                intent.putExtra("data", data);
                startActivity(intent);
                finish();
            });

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pendientes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
