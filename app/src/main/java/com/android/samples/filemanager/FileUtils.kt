/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.samples.filemanager

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader

private const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.provider"

fun getFilesList(selectedItem: File): List<File> {
    val rawFilesList = selectedItem.listFiles()?.filter { !it.isHidden }

    return if (selectedItem == Environment.getExternalStorageDirectory()) {
        rawFilesList?.toList() ?: listOf()
    } else {
        listOf(selectedItem.parentFile) + (rawFilesList?.toList() ?: listOf())
    }
}

fun renderParentLink(activity: AppCompatActivity): String {
    return activity.getString(R.string.go_parent_label)
}

fun renderItem(activity: AppCompatActivity, file: File): String {
    return if (file.isDirectory) {
        activity.getString(R.string.folder_item, file.name)
    } else {
        activity.getString(R.string.file_item, file.name)
    }
}

fun openFile(context: Context, selectedItem: File, dateRoute: String) {
    val uri: Uri = FileProvider.getUriForFile(context, AUTHORITY, selectedItem)
    Toast.makeText(context, "Cargando archivo, por favor espere!!", Toast.LENGTH_LONG).show()

    val intent = Intent(Intent.ACTION_VIEW)
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)




    try {
        context.contentResolver.openInputStream(uri)?.use { inputStream ->
            val reader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            var line: String?

            while (reader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }

            val fileContent = stringBuilder.toString()

            // Process each line and create a Record object
            val recordList = ArrayList<Record>()

            val lines = fileContent.split("\n")
            for (temp in lines) {
                if (temp.isNotEmpty()) {
                    val meterNumber = temp.substring(0, 12).trim()
                    val name = temp.substring(12, 47).trim()
                    val description = temp.substring(47, 62).trim()
                    val location = temp.substring(62, 142).trim()
                    val nr = temp.substring(142, 144).trim().toInt()
                    val localNumber = temp.substring(144, 148).trim().toInt()
                    val maxReading = temp.substring(148, 155).trim().toInt()
                    val lastReading = temp.substring(155, 162).trim().toInt()
                    val previousConsumption = temp.substring(162, 167).trim()

                    val record = Record(meterNumber, name, description, location, nr, localNumber,
                        maxReading, lastReading, -1, previousConsumption)
                    recordList.add(record)
                }
            }

            // Insert records into the database
            SQLHelper(context, null).insertRecord(recordList)

            // Start the List activity (adjust as needed)
            val data = Intent(context, MeterList::class.java)
            data.putExtra("data", dateRoute)
            context.startActivity(data)
        }
    } catch (e: IOException) {
        e.printStackTrace()
        Toast.makeText(context, "Error reading the file", Toast.LENGTH_SHORT).show()
    }
}



