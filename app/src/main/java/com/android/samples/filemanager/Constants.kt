package com.android.samples.filemanager

object Constants {
        const val MANAGE_EXTERNAL_STORAGE_PERMISSION_REQUEST = 1
        const val READ_EXTERNAL_STORAGE_PERMISSION_REQUEST = 2
        }
