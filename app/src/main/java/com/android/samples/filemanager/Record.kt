package com.android.samples.filemanager

import android.annotation.SuppressLint
import java.io.Serializable

/**
 * Created by elias on 10/01/15.
 */
data class Record(
        var meterNumber: String?,
        var name: String?,
        var description: String?,
        var address: String?,
        var routeNumber: Int,
        var locationNumber: Int,
        var maximumReading: Int,
        var lastReading: Int,
        var currentReading: Int,
        var previousConsumption: String?
) : Serializable {

        constructor() : this(
                null,
                null,
                null,
                null,
                0,
                0,
                0,
                0,
                0,
                null
        )

        @SuppressLint("DefaultLocale")
        override fun toString(): String {
            return String.format("%012d", meterNumber?.toLongOrNull()) + String.format("%07d", currentReading) + "\n"
            }
}
