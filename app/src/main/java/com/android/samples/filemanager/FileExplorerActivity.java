package com.android.samples.filemanager;

import static com.android.samples.filemanager.FileUtilsKt.getFilesList;
import static com.android.samples.filemanager.FileUtilsKt.openFile;
import static com.android.samples.filemanager.FileUtilsKt.renderItem;
import static com.android.samples.filemanager.FileUtilsKt.renderParentLink;
import static com.android.samples.filemanager.PermissionUtils.checkStoragePermission;
import static com.android.samples.filemanager.PermissionUtils.requestStoragePermission;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import androidx.appcompat.app.AppCompatActivity;
import com.android.samples.filemanager.databinding.ActivityFileExplorerBinding;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileExplorerActivity extends AppCompatActivity {

    private ActivityFileExplorerBinding binding;
    private File currentDirectory;
    private List<File> filesList;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityFileExplorerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupUi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean hasPermission = checkStoragePermission(this);
        if (hasPermission) {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                if (!Environment.isExternalStorageLegacy()) {
                    binding.rationaleView.setVisibility(View.GONE);
                    binding.legacyStorageView.setVisibility(View.VISIBLE);
                    return;
                }
            }

            binding.rationaleView.setVisibility(View.GONE);
            binding.filesTreeView.setVisibility(View.VISIBLE);
            File downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            open(downloadsDir);
        } else {
            binding.rationaleView.setVisibility(View.VISIBLE);
            binding.filesTreeView.setVisibility(View.GONE);
        }
    }

    private void setupUi() {
        binding.permissionButton.setOnClickListener(v -> requestStoragePermission(this));

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<>());
        binding.filesTreeView.setAdapter(adapter);
        binding.filesTreeView.setOnItemClickListener((parent, view, position, id) -> {
        File selectedItem = filesList.get(position);
        open(selectedItem);
    });
    }

    private void open(File selectedItem) {
        if (selectedItem.isFile()) {
            String dateRoute = getIntent().getStringExtra("data");
            assert dateRoute != null;
            openFile(this, selectedItem, dateRoute);
            finish();
            return;
        }

        currentDirectory = selectedItem;
        filesList = getFilesList(currentDirectory);

        adapter.clear();
        adapter.addAll(getFormattedFileList());

        adapter.notifyDataSetChanged();
    }

    private List<String> getFormattedFileList() {
        List<String> formattedList = new ArrayList<>();
        for (File file : filesList) {
        if (file.getPath().equals(Objects.requireNonNull(currentDirectory.getParentFile()).getPath())) {
            formattedList.add(renderParentLink(this));
        } else {
            formattedList.add(renderItem(this, file));
        }
    }
        return formattedList;
    }
}
