package com.android.samples.filemanager;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.android.samples.filemanager.databinding.ActivityRutaBinding;

public class Route extends AppCompatActivity {

    private EditText txtMonth;
    private EditText txtYear;
    private TextView lblCalendarDay;
    private TextView lblCalendarMonth;
    private TextView lblCalendarYear;
    public int periodMonth;
    public int periodYear;
    public String closingFile;
    public EditText nRoute;
    private int mYear, mMonth, mDay;
    static final int DATE_DIALOG_ID = 0;
    public static Boolean flag = false; // this variable is set if there is a file loaded in memory
    public static Boolean searchFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.android.samples.filemanager.databinding.ActivityRutaBinding binding = ActivityRutaBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        txtMonth = findViewById(R.id.txt_mes_periodo);
        //este campo es para probar otro ttf.
        txtYear = findViewById(R.id.txt_ano_periodo);
        //private Button btnChangeDate;//cambia la fecha actual de la lectura
        //hace lo mismo que el de arriba pero con una imagen
        ImageView btnChangeDate = findViewById(R.id.btn_cambiar_fecha);
        lblCalendarDay = findViewById(R.id.lbl_dia_calendario);
        lblCalendarMonth = findViewById(R.id.lbl_mes_calendario);
        lblCalendarYear = findViewById(R.id.lbl_ano_calendario);
        nRoute = findViewById(R.id.txt_ruta);

        //me poseciono en este editable
        nRoute.requestFocus();
        //muestro el teclado en este editable
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        //abro un objeto calendario para obtener los valores de año, dia y mes
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // add a click listener to the button
        //le doy la funcion de levantar el calendario al bottom
        btnChangeDate.setOnClickListener(v -> showDialog(DATE_DIALOG_ID));
        //se llama al metodo para actualizar las vistas de los campos de las fechas.
        updateDisplay();

    }

    //this method open the file explorer
    public void onClickExplorer(View v) {

        flag = false;
        searchFlag = false;
        //variable que almacena los datos de la ruta y periodo para pasarlos por parametro a otra vista.
        String data = nRoute.getText().toString() + "," + txtMonth.getText().toString() + "," + txtYear.getText().toString();

        //valida que la ruta no venga limpia
        if(nRoute.getText().toString().equals("")){

            Toast toast2 =
                    Toast.makeText(getApplicationContext(),
                            "Digitar código de sector!!!", Toast.LENGTH_SHORT);

            toast2.setGravity(Gravity.TOP| Gravity.CENTER,0,200);
            toast2.show();
            return;
        }

        //se crea un objeto de la clase que manipula la base de datos
        SQLHelper helper= new SQLHelper(this, null);
        //llamo al method para buscar registros y confirmar si hay o no datos en memoria.
        Record resultado = helper.searchGeneralRecords();

        //valido que haya o no datos en memoria
        if(resultado != null) {

            //filtro los registros por el numero de ruta para ver si este tiene lecturas pendientes.
            Record obj = helper.searchRecordByRoute(Integer.parseInt(nRoute.getText().toString()));

            //valido que el resultado no venga null, si fuera asi entonces no tiene lecturas pendientes
            //de lo contrario carga el layout de Lista de Registros
            if(obj != null) {
                flag = true;
                Intent intent = new Intent(Route.this, MeterList.class);
                intent.putExtra("data", data);
                startActivity(intent);
                nRoute.setText("");
            }else {
                Toast toast2 =
                        Toast.makeText(getApplicationContext(),
                                "La ruta no tiene lecturas pendientes!!!", Toast.LENGTH_SHORT);

                toast2.setGravity(Gravity.TOP| Gravity.CENTER,0,200);
                toast2.show();
            }

        }
        else {
            //en caso de que no hayan archivos en memoria se abre el explorador para buscar el archivo con los datos de los clientes
            //hospedado en alguna carpeta y de ahi abro el layout de ruta por lo que aqui se cierra este layout al final.
            Intent file_explorer = new Intent(Route.this, FileExplorerActivity.class);
            file_explorer.putExtra("data", data);
            startActivityForResult(file_explorer, 555);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        //se crea el dialogo con el calendario
        if (id == DATE_DIALOG_ID) {
            return new DatePickerDialog(this,
                    mDateSetListener,
                    mYear, (mMonth), mDay);
        }
        return null;
    }

    // updates the date we display in the TextView
    private void updateDisplay() {

        //se actualizan todos los indicadores del layout.
        periodMonth = mMonth + 1;
        periodYear = mYear;

        if(mDay < 15){
            periodMonth = periodMonth - 1;

            if(periodMonth == 0){
                periodMonth = 12;
                periodYear = periodYear -1;
            }
        }

        txtMonth.setText(new StringBuilder().append(periodMonth));
        txtYear.setText(new StringBuilder().append(periodYear));

        lblCalendarYear.setText(new StringBuilder()
                        // Month is 0 based so add 1
                        .append(mYear).append(" "));
        lblCalendarMonth.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(mMonth + 1).append(" "));
        lblCalendarDay.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(mDay).append(" "));

    }

    // the callback received when the user "sets" the date in the dialog
    private final DatePickerDialog.OnDateSetListener mDateSetListener =

            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,

                                      int monthOfYear, int dayOfMonth) {

                    mYear = year;

                    mMonth = monthOfYear;

                    mDay = dayOfMonth;

                    updateDisplay();

                }

            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ruta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        String data = txtMonth.getText().toString() + "," + txtYear.getText().toString();
        if (item.getItemId() == R.id.lbl_buscar) {
            searchFlag = true;
            String dataSearch = txtMonth.getText().toString() + "," + txtYear.getText().toString();
            Intent intent = new Intent(Route.this, MeterList.class);
            intent.putExtra("dataSearch", dataSearch);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.lbl_ruta_pendiente) {

            Intent pend = new Intent(Route.this, Pendientes.class);
            pend.putExtra("data", data);
            startActivity(pend);
            return true;
        }
        if (item.getItemId() == R.id.lbl_cierre) {

            SQLHelper obj = new SQLHelper(this, null);
            if (obj.searchCompletedRecords() == null) {
                Toast toast2 =
                        Toast.makeText(getApplicationContext(),
                                "No hay registros completos...", Toast.LENGTH_SHORT);

                toast2.setGravity(Gravity.TOP | Gravity.CENTER, 0, 200);
                toast2.show();
                return false;

            } else {

                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("CIERRE")
                        .setMessage("Esta seguro de generar cierre?")
                        .setNegativeButton(android.R.string.cancel, null)//sin listener
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {//un listener que al pulsar, cierre la aplicacion
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                generateClosing();
                                sendEmail();

                            }
                        })
                        .show();
                return true;
            }
        }

        return super.onContextItemSelected(item);

    }

    protected void sendEmail() {

        //un listener que al pulsar, cierre la aplicacion
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("CORREO")
                .setMessage("Desea enviar cierre por correo?")
                .setNegativeButton(android.R.string.cancel, null)//sin listener
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    SQLHelper obj = new SQLHelper(Route.this,null);
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("message/*");

                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Cierre del " + closingFile);

                    if(obj.searchPendingRecords()==true) {
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Adjunto archivo cierre parcial.");

                    }else {
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Adjunto archivo cierre final.");
                    }


                    Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Collector/", closingFile));

                    emailIntent.putExtra(Intent.EXTRA_STREAM, uri);

                    try {
                        startActivity(emailIntent);
                    } catch (android.content.ActivityNotFoundException ex) {

                        Toast.makeText(Route.this,
                                "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                })
                .show();


    }

    public void generateClosing(){

        SQLHelper obj = new SQLHelper(this, null);
        ArrayList<Record> recordList = new ArrayList<>(obj.searchCompletedRecords());

        try
        {
            File sdRoute = Environment.getExternalStorageDirectory();

            Date date = new Date();
            DateFormat dateTitle = new SimpleDateFormat("ddMMyyyyHHmm");
            String fielNameDate = dateTitle.format(date);

            File f = new File(sdRoute.getAbsolutePath() + "/Collector/");
            boolean success = false;
            if(!f.exists()){
                success = f.mkdir();
            }
            if (!success){
                Log.d("main","Folder not created.");
            }
            else{
                Log.d("main","Folder created!");
            }

            File file = new File(f.getAbsolutePath(), fielNameDate + ".txt");
            OutputStreamWriter fout =
                    new OutputStreamWriter(
                            new FileOutputStream(file));

            for(int i=0; i < recordList.size(); i++) {

                fout.write( recordList.get(i).toString());
            }
            //se encapsula en variable para buscar a la hora de enviar el mail
            closingFile = fielNameDate + ".txt";
            fout.close();
            this.deleteRecords(recordList);
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
        }
    }// end Guardar


    public void deleteRecords(ArrayList<Record> pLista){

        SQLHelper obj = new SQLHelper(this,null);

        for(int i=0; i < pLista.size(); i++) {

            obj.deleteRecord(pLista.get(i).getMeterNumber());
        }

    }

}