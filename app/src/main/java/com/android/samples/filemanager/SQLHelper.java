package com.android.samples.filemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elias on 18/01/15.
 */
public class SQLHelper extends SQLiteOpenHelper{

    //SETTINGS DE LA BASE DE DATOS
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "acueducto.db";
    //definir tabla o tablas
    private static final String TABLA_REGISTRO = "registro";
    //definir columnas
    private static final String COLUMNA_MEDIDOR  = "nMedidor";
    private static final String COLUMNA_NOMBRE = "nombre";
    private static final String COLUMNA_DESCRIPCION= "descripcion";
    private static final String COLUMNA_DONDE  = "donde";
    private static final String COLUMNA_RUTA = "nRuta";
    private static final String COLUMNA_LOCALIZACION= "nLocalizacion";
    private static final String COLUMNA_LECTURA_MAXIMA  = "maxLectura";
    private static final String COLUMNA_ULTIMA_LECTURA = "ultimaLectura";
    private static final String COLUMNA_LECTURA_ACTUAL= "lecturaActual";
    private static final String COLUMNA_CONSUMO_ANTERIOR= "consumoAnterior";

    //CAMBIOS
    private String SQLUpdateV2 = "ALTER TABLE " + TABLA_REGISTRO + " ADD COLUMN " + COLUMNA_CONSUMO_ANTERIOR + " TEXT";

    public SQLHelper(Context context, CursorFactory factory) {
        //llamar a la super clase de base de datos
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String query =  "CREATE TABLE " + TABLA_REGISTRO +
                  " ("+ COLUMNA_MEDIDOR + " INTEGER PRIMARY KEY, " +
                        COLUMNA_NOMBRE + " TEXT," +
                        COLUMNA_DESCRIPCION + " TEXT," +
                        COLUMNA_DONDE + " TEXT," +
                        COLUMNA_RUTA + " INTEGER," +
                        COLUMNA_LOCALIZACION + " INTEGER," +
                        COLUMNA_LECTURA_MAXIMA + " INTEGER," +
                        COLUMNA_ULTIMA_LECTURA + " INTEGER," +
                        COLUMNA_LECTURA_ACTUAL + " INTEGER)";


        db.execSQL(query);
        db.execSQL(SQLUpdateV2);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int last, int current) {
        if(last == 1 && current == 2){
            db.execSQL(SQLUpdateV2);
        }else {
            db.execSQL("DROP TABLE IF EXISTS " + TABLA_REGISTRO);
            onCreate(db);
        }

    }

    //metodo para insertar un registro
    public void insertRecord(ArrayList<Record> records) {



        for(int x = 0; x < records.size(); x++) {

            try{Thread.sleep(10);} catch (Exception e){}

            ContentValues values = new ContentValues();

            values.put(COLUMNA_MEDIDOR, records.get(x).getMeterNumber());
            values.put(COLUMNA_NOMBRE, records.get(x).getName());
            values.put(COLUMNA_DESCRIPCION, records.get(x).getDescription());
            values.put(COLUMNA_DONDE, records.get(x).getAddress());
            values.put(COLUMNA_RUTA, records.get(x).getRouteNumber());
            values.put(COLUMNA_LOCALIZACION, records.get(x).getLocationNumber());
            values.put(COLUMNA_LECTURA_MAXIMA, records.get(x).getMaximumReading());
            values.put(COLUMNA_ULTIMA_LECTURA, records.get(x).getLastReading());
            values.put(COLUMNA_LECTURA_ACTUAL, records.get(x).getCurrentReading());
            values.put(COLUMNA_CONSUMO_ANTERIOR, records.get(x).getPreviousConsumption());

            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(TABLA_REGISTRO, " ", values);
            db.close();
        }
    }

    public void deleteAllRecords() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLA_REGISTRO,null,null);
    }

    public boolean save(String meter, int currentReading){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues update_registro = new ContentValues();
        update_registro.put(COLUMNA_MEDIDOR,meter);
        update_registro.put(COLUMNA_LECTURA_ACTUAL,currentReading);

        try {
            db.update(TABLA_REGISTRO, update_registro, "nMedidor = '" + meter + "'", null);
            return true;

        }catch (Exception e) {

            return false;

        }
    }

    public Record search(String meter) {

        String query = "SELECT * FROM " + TABLA_REGISTRO + " WHERE " + COLUMNA_MEDIDOR
                + " =  '"  + meter + "'"  ;

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        Record result;

        if(cursor.moveToFirst()) {

            result = new Record();
            cursor.moveToFirst();
            loadFromSQLFormat(cursor, result);
            cursor.close();
        }
        else {
            result = null;
        }
        db.close();
        return result;
    }

    private static void loadFromSQLFormat(Cursor cursor, Record result) {
        result.setMeterNumber(cursor.getString(0));
        result.setName(cursor.getString(1));
        result.setDescription(cursor.getString(2));
        result.setAddress(cursor.getString(3));
        result.setRouteNumber(Integer.parseInt(cursor.getString(4)));
        result.setLocationNumber(Integer.parseInt(cursor.getString(5)));
        result.setMaximumReading(Integer.parseInt(cursor.getString(6)));
        result.setLastReading(Integer.parseInt(cursor.getString(7)));
        result.setCurrentReading(Integer.parseInt(cursor.getString(8)));
        result.setPreviousConsumption(cursor.getString(9));
    }

    public List<Integer> searchPendingRoutes() {

        List<Integer> lista = new ArrayList<Integer>();

        String query = "SELECT DISTINCT " + COLUMNA_RUTA + " FROM " + TABLA_REGISTRO + " WHERE " + COLUMNA_LECTURA_ACTUAL + " = -1 ORDER BY " + COLUMNA_RUTA + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                int x = cursor.getInt(0);
                lista.add(x);
                cursor.moveToNext();

            }
        }else {
            lista = null;
        }
        db.close();
        cursor.close();
        return lista;



    }

    public Record searchRecordByRoute(int pRuta) {

        String query = "SELECT * FROM " + TABLA_REGISTRO + " WHERE " + COLUMNA_RUTA + " = " + pRuta + " AND " + COLUMNA_LECTURA_ACTUAL + " = -1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        Record result;

        if(cursor.moveToFirst()) {

            result = new Record();
            cursor.moveToFirst();
            loadFromSQLFormat(cursor, result);
            cursor.close();
        }
        else {
            result = null;
        }
        db.close();
        return result;
    }

    public Record searchGeneralRecords() {

        String query = "SELECT * FROM " + TABLA_REGISTRO;

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        Record result;

        if(cursor.moveToFirst()) {

            result = new Record();
            cursor.moveToFirst();
            loadFromSQLFormat(cursor, result);
            cursor.close();
        }
        else {
            result = null;
        }
        db.close();
        return result;
    }

    public Record nextRecord(int pRuta) {


        String query = "SELECT t.* FROM " + TABLA_REGISTRO + " t WHERE t." + COLUMNA_RUTA
                + " = "  +  pRuta + " AND t."+ COLUMNA_LECTURA_ACTUAL + " = -1" + " ORDER BY nLocalizacion ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        Record result;

        if(cursor.moveToFirst()) {

            result = new Record();
            cursor.moveToFirst();
            loadFromSQLFormat(cursor, result);
            cursor.close();
        }
        else {
            result = null;
        }
        db.close();
        return result;
    }

    public void deleteRecord(String idMeter) {

            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL("DELETE FROM " + TABLA_REGISTRO + " WHERE " + COLUMNA_MEDIDOR + " = " + idMeter);

    }

    public List<Record> searchCompletedRecords() {

        List<Record> recordList = new ArrayList<Record>();
        Record result;

        String query = "SELECT * FROM " + TABLA_REGISTRO + " WHERE " + COLUMNA_LECTURA_ACTUAL + " != -1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                result = new Record();
                loadFromSQLFormat(cursor, result);
                recordList.add(result);

                cursor.moveToNext();

            }
        }else {
            recordList = null;
        }
        db.close();
        cursor.close();
        return recordList;



    }
    //buscar registro general para validar si todos los datos ya fueron recolectados o hay pendientes
    public boolean searchPendingRecords() {

        String query = "SELECT * FROM " + TABLA_REGISTRO + " WHERE " + COLUMNA_LECTURA_ACTUAL + " = -1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        //El cursor guarda la cantidad de registros encontrados en la sentenia
        Cursor cursor = db.rawQuery(query, null);

        Record result;

        if(cursor.moveToFirst()) {

            result = new Record();
            cursor.moveToFirst();
            loadFromSQLFormat(cursor, result);
            cursor.close();
        }
        else {
            this.deleteAllRecords();
            return false;
        }
        db.close();
        return true;
    }

}
